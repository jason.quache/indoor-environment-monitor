/* Helper functions for dealing with rounding, conversions etc */
#include <stdint.h>

/* Convert float to char 

float x is converted to a char array and this is stored in the char array
pointed at by p. char_len is the length of the char array
e.g. if want to get 21.21 --> need char_len = 5 (5 characters)
*/
// Credit: https://stackoverflow.com/questions/23191203/convert-float-to-string-without-sprintf
char * _float_to_char(float x, char *p, uint8_t char_len) {
    char *s = p + char_len; // go to end of buffer
    uint16_t decimals;  // variable to store the decimals
    int units;  // variable to store the units (part to left of decimal place)
    if (x < 0) { // take care of negative numbers
        decimals = (int)(x * -100) % 100; // make 1000 for 3 decimals etc.
        units = (int)(-1 * x);
    } else { // positive numbers
        decimals = (int)(x * 100) % 100;
        units = (int)x;
    }

    *--s = (decimals % 10) + '0';
    decimals /= 10; // repeat for as many decimal places as you need
    *--s = (decimals % 10) + '0';
    *--s = '.';

    while (units > 0) {
        *--s = (units % 10) + '0';
        units /= 10;
    }
    if (x < 0) *--s = '-'; // unary minus sign for negative numbers
    return s;
}

/* Convert int to char 

int x is converted to a char array and this is stored in the char array
pointed at by p. char_len is the length of the char array
e.g. if want to get 21.21 --> need char_len = 5 (5 characters)
*/
char * _int_to_char(int x, char *p, uint8_t char_len) {
    char *s = p + char_len; // go to end of buffer
    int units = x;  // variable to store the units (part to left of decimal place)

    while (units > 0) {
        *--s = (units % 10) + '0';
        units /= 10;
    }
    return s;
		
	
}

/* Round integers to nearest 10, 100, etc 

round_to = 10 to round to nearest 10
round_to = 100 to round to nearest 100 etc*/
int round(uint32_t num, uint16_t round_to)
{
    // Smaller multiple
    uint32_t a = (num / round_to) * round_to;
     
    // Larger multiple
    uint32_t b = a + round_to;
 
    // Return of closest of two
    return (num - a >= b - num)? b : a;
}