/*
	Authored 2016-2018. Phillip Stanley-Marbell, Youchao Wang, James Meech.

	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:

	*	Redistributions of source code must retain the above
		copyright notice, this list of conditions and the following
		disclaimer.

	*	Redistributions in binary form must reproduce the above
		copyright notice, this list of conditions and the following
		disclaimer in the documentation and/or other materials
		provided with the distribution.

	*	Neither the name of the author nor the names of its
		contributors may be used to endorse or promote products
		derived from this software without specific prior written
		permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
	FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
	COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
	BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
	LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdlib.h>

#include "fsl_misc_utilities.h"
#include "fsl_device_registers.h"
#include "fsl_i2c_master_driver.h"
#include "fsl_spi_master_driver.h"
#include "fsl_rtc_driver.h"
#include "fsl_clock_manager.h"
#include "fsl_power_manager.h"
#include "fsl_mcglite_hal.h"
#include "fsl_port_hal.h"

#include "gpio_pins.h"
#include "SEGGER_RTT.h"
#include "warp.h"


extern volatile WarpI2CDeviceState	deviceBME680State;
extern volatile uint8_t			deviceBME680CalibrationValues[];
extern volatile uint32_t		gWarpI2cBaudRateKbps;
extern volatile uint32_t		gWarpI2cTimeoutMilliseconds;
extern volatile uint32_t		gWarpSupplySettlingDelayMilliseconds;


void
initBME680(const uint8_t i2cAddress, WarpI2CDeviceState volatile *  deviceStatePointer)
{
	deviceStatePointer->i2cAddress	= i2cAddress;
	deviceStatePointer->signalType	= (kWarpTypeMaskPressure | kWarpTypeMaskTemperature);

	return;
}

WarpStatus
writeSensorRegisterBME680(uint8_t deviceRegister, uint8_t payload)
{
	uint8_t		payloadByte[1], commandByte[1];
	i2c_status_t	status;

	if (deviceRegister > 0xFF)
	{
		return kWarpStatusBadDeviceCommand;
	}

	i2c_device_t slave =
	{
		.address = deviceBME680State.i2cAddress,
		.baudRate_kbps = gWarpI2cBaudRateKbps
	};

	commandByte[0] = deviceRegister;
	payloadByte[0] = payload;
	status = I2C_DRV_MasterSendDataBlocking(
							0 /* I2C instance */,
							&slave,
							commandByte,
							1,
							payloadByte,
							1,
							gWarpI2cTimeoutMilliseconds);
	if (status != kStatus_I2C_Success)
	{
		return kWarpStatusDeviceCommunicationFailed;
	}

	return kWarpStatusOK;
}

WarpStatus
readSensorRegisterBME680(uint8_t deviceRegister, int numberOfBytes)
{
	uint8_t		cmdBuf[1] = {0xFF};
	i2c_status_t	status;


	USED(numberOfBytes);

	/*
	 *	We only check to see if it is past the config registers.
	 *
	 *	TODO: We should eventually numerate all the valid register addresses
	 *	(configuration, control, and calibration) here.
	 */
	if (deviceRegister > kWarpSensorConfigurationRegisterBME680CalibrationRegion2End)
	{
		return kWarpStatusBadDeviceCommand;
	}

	i2c_device_t slave =
	{
		.address = deviceBME680State.i2cAddress,
		.baudRate_kbps = gWarpI2cBaudRateKbps
	};

	cmdBuf[0] = deviceRegister;

	status = I2C_DRV_MasterReceiveDataBlocking(
							0 /* I2C peripheral instance */,
							&slave,
							cmdBuf,
							1,
							(uint8_t *)deviceBME680State.i2cBuffer,
							1,
							gWarpI2cTimeoutMilliseconds);

	if (status != kStatus_I2C_Success)
	{
		return kWarpStatusDeviceCommunicationFailed;
	}

	return kWarpStatusOK;
}

/* Configure the BME680 - sets the oversampling rates for temp, pressure, hum 
						  and sets the operation mode (sleep / forced)
						  AND read the calibration registers
*/
WarpStatus
configureSensorBME680(uint8_t payloadCtrl_Hum, uint8_t payloadCtrl_Meas, uint8_t payloadGas_0)
{
	uint8_t		reg, index = 0;
	WarpStatus	status1, status2, status3, status4 = 0;

	/* ctrl_hum (0x72) register
	   ------------------------
	   bits <2:0> --> humidity oversampling setting (101, 100, 011, 010, 001, 000 for x16, x8, x4, x2, x1, skip respectively)
	*/
	status1 = writeSensorRegisterBME680(kWarpSensorConfigurationRegisterBME680Ctrl_Hum,
							payloadCtrl_Hum);


	/* ctrl_meas (0x74) register
	   -------------------------
	   bits <1:0> --> mode (00 for sleep mode, 01 for forced mode)
	   bits <4:2> --> pressure oversampling setting (101, 100, 011, 010, 001, 000 for x16, x8, x4, x2, x1, skip respectively)
	   bits <7:5> --> temp oversampling settings (101, 100, 011, 010, 001, 000 for x16, x8, x4, x2, x1, skip respectively)
	*/
	status2 = writeSensorRegisterBME680(kWarpSensorConfigurationRegisterBME680Ctrl_Meas,
							payloadCtrl_Meas);

	/* ctrl_gas_0 (0x70) register
	   -------------------------
	   bit <3> --> turn heater off (set to 1)
	*/
	status3 = writeSensorRegisterBME680(kWarpSensorConfigurationRegisterBME680Ctrl_Gas_0,
							payloadGas_0);

	/*
	 *	Read the calibration registers
	 */
	// Calibration region 1 is from 0x89 to 0xA2 (not including 0xA2)
	for (	reg = kWarpSensorConfigurationRegisterBME680CalibrationRegion1Start;
		reg < kWarpSensorConfigurationRegisterBME680CalibrationRegion1End;
		reg++)
	{
		status4 |= readSensorRegisterBME680(reg, 1 /* numberOfBytes */);
		deviceBME680CalibrationValues[index++] = deviceBME680State.i2cBuffer[0];
	}
	// Calibration region 2 is from 0xE1 to 0xF2 (not including 0xF2)
	for (	reg = kWarpSensorConfigurationRegisterBME680CalibrationRegion2Start;
		reg < kWarpSensorConfigurationRegisterBME680CalibrationRegion2End;
		reg++)
	{
		status4 |= readSensorRegisterBME680(reg, 1 /* numberOfBytes */);
		deviceBME680CalibrationValues[index++] = deviceBME680State.i2cBuffer[0];
	}

	return (status1 | status2 | status3 | status4);
}

// Calculate the temperature in deg C from the raw ADC value
// Adapted from datasheet pg 18 / official BME680 open-source driver
int16_t 
calcTempBME680(uint32_t temp_adc)
{
	int64_t var1;
	int64_t var2;
	int64_t var3;
	int32_t par_t1;  // at address 0xE9 (LSB) / 0xEA (MSB)
	int32_t par_t2;  // at address 0x8A (LSB) / 0x8B (MSB)
	int32_t par_t3;  // at address 0x8C
	int32_t t_fine;
	int16_t calc_temp;

	// par_t1 = (par_t1_MSB << 8) | par_t1_LSB
	par_t1 = (deviceBME680CalibrationValues[34] << 8) | deviceBME680CalibrationValues[33];
	// par_t2 = (par_t2_MSB << 8) | par_t2_LSB
	par_t2 = (deviceBME680CalibrationValues[2] << 8) | deviceBME680CalibrationValues[1];
	// par_t3
	par_t3 = deviceBME680CalibrationValues[3];


	var1 = ((int32_t) temp_adc >> 3) - ((int32_t) par_t1 << 1);
	var2 = (var1 * (int32_t) par_t2) >> 11;
	var3 = ((var1 >> 1) * (var1 >> 1)) >> 12;
	var3 = ((var3) * ((int32_t) par_t3 << 4)) >> 14;
	t_fine = (int32_t) (var2 + var3);
	calc_temp = (int16_t) (((t_fine * 5) + 128) >> 8);

	return calc_temp;
}


// Calculate the humidity % (in integer form so 100% = 100000)
// Adapted from datasheet pg 21 / official BME680 open-source driver
// temp_comp is the calculated temperature output by calcTempBME680
uint32_t
calcHumidityBME680(uint16_t hum_adc, uint16_t temp_comp)
{
	int32_t var1;
	int32_t var2;
	int32_t var3;
	int32_t var4;
	int32_t var5;
	int32_t var6;
	int32_t temp_scaled;

	int32_t par_h1;  // at address 0xE2 <7:4> (LSB) / 0xE3 (MSB)
	int32_t par_h2;  // at address 0xE2 <7:4> (LSB) / 0xE1 (MSB)
	int32_t par_h3;  // at address 0xE4
	int32_t par_h4;  // at address 0xE5
	int32_t par_h5;  // at address 0xE6
	int32_t par_h6;  // at address 0xE7
	int32_t par_h7;  // at address 0xE8

	int32_t calc_hum;

	// Retrieve the calibration parameters
	// par_h1 = (par_h1_MSB << 4) | (par_h1_LSB >> 4)
	par_h1 = (deviceBME680CalibrationValues[27] << 4) | (deviceBME680CalibrationValues[26] >> 4);
	// par_h2 = (par_h2_MSB << 4) | (par_h2_LSB >> 4)
	par_h2 = (deviceBME680CalibrationValues[25] << 4) | (deviceBME680CalibrationValues[26] >> 4);
	// par_h3
	par_h3 = deviceBME680CalibrationValues[28];
	// par_h4
	par_h4 = deviceBME680CalibrationValues[29];
	// par_h5
	par_h5 = deviceBME680CalibrationValues[30];
	// par_h6
	par_h6 = deviceBME680CalibrationValues[31];
	// par_h7
	par_h7 = deviceBME680CalibrationValues[32];


	temp_scaled = (int32_t)temp_comp;
	var1 = (int32_t) (hum_adc - ((int32_t) ((int32_t)par_h1 * 16)))
		- (((temp_scaled * (int32_t)par_h3) / ((int32_t) 100)) >> 1);
	var2 = ((int32_t)par_h2
		* (((temp_scaled * (int32_t)par_h4) / ((int32_t) 100))
			+ (((temp_scaled * ((temp_scaled * (int32_t)par_h5) / ((int32_t) 100))) >> 6)
				/ ((int32_t) 100)) + (int32_t) (1 << 14))) >> 10;
	var3 = var1 * var2;
	var4 = (int32_t)par_h6 << 7;
	var4 = ((var4) + ((temp_scaled * (int32_t)par_h7) / ((int32_t) 100))) >> 4;
	var5 = ((var3 >> 14) * (var3 >> 14)) >> 10;
	var6 = (var4 * var5) >> 1;
	calc_hum = (((var3 + var6) >> 10) * ((int32_t) 1000)) >> 12;

	if (calc_hum > 100000) /* Cap at 100%rH */
		calc_hum = 100000;
	else if (calc_hum < 0)
		calc_hum = 0;

	return (uint32_t) calc_hum;
}


void
printSensorDataBME680(bool hexModeFlag)
{
	uint16_t	readSensorRegisterValueLSB;
	uint16_t	readSensorRegisterValueMSB;
	uint16_t	readSensorRegisterValueXLSB;
	uint32_t	unsignedRawAdcValue;
	uint16_t    tempdegC;
	uint32_t    hum_percentage;

	WarpStatus	triggerStatus, i2cReadStatusMSB, i2cReadStatusLSB, i2cReadStatusXLSB;


	/*
	 *	First, trigger a measurement
	 *
	 * ctrl_meas is set to 001 001 01
	 * 01  --> forced mode
	 * 001 --> x1 pressure oversampling
	 * 001 --> x1 temp oversampling
	*/
	triggerStatus = writeSensorRegisterBME680(kWarpSensorConfigurationRegisterBME680Ctrl_Meas,
							0b00100101);

	/* PRESSURE MEASUREMENT */
	i2cReadStatusMSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680press_msb, 1);
	readSensorRegisterValueMSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680press_lsb, 1);
	readSensorRegisterValueLSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusXLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680press_xlsb, 1);
	readSensorRegisterValueXLSB = deviceBME680State.i2cBuffer[0];
	unsignedRawAdcValue =
			((readSensorRegisterValueMSB & 0xFF)  << 12) |
			((readSensorRegisterValueLSB & 0xFF)  << 4)  |
			((readSensorRegisterValueXLSB & 0xF0) >> 4);

	if ((triggerStatus != kWarpStatusOK) || (i2cReadStatusMSB != kWarpStatusOK) || (i2cReadStatusLSB != kWarpStatusOK) || (i2cReadStatusXLSB != kWarpStatusOK))
	{
		SEGGER_RTT_WriteString(0, " ----,");
	}
	else
	{
		if (hexModeFlag)
		{
			SEGGER_RTT_printf(0, " 0x%02x 0x%02x 0x%02x,", readSensorRegisterValueMSB, readSensorRegisterValueLSB, readSensorRegisterValueXLSB);
		}
		else
		{
			SEGGER_RTT_printf(0, " %u,", unsignedRawAdcValue);
		}
	}

	/* TEMPERATURE MEASUREMENT */
	i2cReadStatusMSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680temp_msb, 1);
	readSensorRegisterValueMSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680temp_lsb, 1);
	readSensorRegisterValueLSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusXLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680temp_xlsb, 1);
	readSensorRegisterValueXLSB = deviceBME680State.i2cBuffer[0];
	// Raw measurement is 20 bits
	unsignedRawAdcValue =
			((readSensorRegisterValueMSB & 0xFF)  << 12) |
			((readSensorRegisterValueLSB & 0xFF)  << 4)  |
			((readSensorRegisterValueXLSB & 0xF0) >> 4);

	// Convert the raw temp value to an integer (see pg 18 of BME680 datasheet)
	tempdegC = calcTempBME680(unsignedRawAdcValue);
	if ((triggerStatus != kWarpStatusOK) || (i2cReadStatusMSB != kWarpStatusOK) || (i2cReadStatusLSB != kWarpStatusOK) || (i2cReadStatusXLSB != kWarpStatusOK))
	{
		SEGGER_RTT_WriteString(0, " ----,");
	}
	else
	{
		if (hexModeFlag)
		{
			SEGGER_RTT_printf(0, " 0x%02x 0x%02x 0x%02x,", readSensorRegisterValueMSB, readSensorRegisterValueLSB, readSensorRegisterValueXLSB);
		}
		else
		{
			//SEGGER_RTT_printf(0, " %u,", unsignedRawAdcValue);
			SEGGER_RTT_printf(0, " %u deg C,", tempdegC);
			// SEGGER_RTT_printf(0, " %f deg C,", tempdegC_Float);
		}
	}

	/* HUMIDITY MEASUREMENT */
	i2cReadStatusMSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680hum_msb, 1);
	readSensorRegisterValueMSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680hum_lsb, 1);
	readSensorRegisterValueLSB = deviceBME680State.i2cBuffer[0];
	unsignedRawAdcValue = ((readSensorRegisterValueMSB & 0xFF) << 8) | (readSensorRegisterValueLSB & 0xFF);
	// Convert the raw value to the integer % relative humidity
	hum_percentage = calcHumidityBME680(unsignedRawAdcValue, tempdegC);

	if ((triggerStatus != kWarpStatusOK) || (i2cReadStatusMSB != kWarpStatusOK) || (i2cReadStatusLSB != kWarpStatusOK))
	{
		SEGGER_RTT_WriteString(0, " ----,");
	}
	else
	{
		if (hexModeFlag)
		{
			SEGGER_RTT_printf(0, " 0x%02x 0x%02x,", readSensorRegisterValueMSB, readSensorRegisterValueLSB);
		}
		else
		{
			// SEGGER_RTT_printf(0, " %u,", unsignedRawAdcValue);
			SEGGER_RTT_printf(0, " %u %%,", hum_percentage);
		}
	}
}


/* Get the temperature and humidity readings from BME680,
   save them to the temp and hum variables.
*/
void
getReadingsBME680(uint16_t* temp, uint32_t* hum)
{
	uint16_t	readSensorRegisterValueLSB;
	uint16_t	readSensorRegisterValueMSB;
	uint16_t	readSensorRegisterValueXLSB;
	uint32_t	unsignedRawAdcValue;
	uint16_t    tempdegC;
	uint32_t    hum_percentage;

	WarpStatus	triggerStatus, i2cReadStatusMSB, i2cReadStatusLSB, i2cReadStatusXLSB;

	/*
	 *	First, trigger a measurement
	 *
	 * ctrl_meas is set to 001 001 01
	 * 01  --> forced mode
	 * 001 --> x1 pressure oversampling
	 * 001 --> x1 temp oversampling
	*/
	triggerStatus = writeSensorRegisterBME680(kWarpSensorConfigurationRegisterBME680Ctrl_Meas,
							0b00100101);

	/* TEMPERATURE MEASUREMENT */
	i2cReadStatusMSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680temp_msb, 1);
	readSensorRegisterValueMSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680temp_lsb, 1);
	readSensorRegisterValueLSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusXLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680temp_xlsb, 1);
	readSensorRegisterValueXLSB = deviceBME680State.i2cBuffer[0];
	// Raw measurement is 20 bits
	unsignedRawAdcValue =
			((readSensorRegisterValueMSB & 0xFF)  << 12) |
			((readSensorRegisterValueLSB & 0xFF)  << 4)  |
			((readSensorRegisterValueXLSB & 0xF0) >> 4);

	// Convert the raw temp value to an integer (see pg 18 of BME680 datasheet)
	tempdegC = calcTempBME680(unsignedRawAdcValue);


	/* HUMIDITY MEASUREMENT */
	i2cReadStatusMSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680hum_msb, 1);
	readSensorRegisterValueMSB = deviceBME680State.i2cBuffer[0];
	i2cReadStatusLSB = readSensorRegisterBME680(kWarpSensorOutputRegisterBME680hum_lsb, 1);
	readSensorRegisterValueLSB = deviceBME680State.i2cBuffer[0];
	unsignedRawAdcValue = ((readSensorRegisterValueMSB & 0xFF) << 8) | (readSensorRegisterValueLSB & 0xFF);
	// Convert the raw value to the integer % relative humidity
	hum_percentage = calcHumidityBME680(unsignedRawAdcValue, tempdegC);

	// Update value pointed to by temp and hum
	*temp = tempdegC;
	*hum = hum_percentage;
}

