/* OLED SSD1331 Driver Implementation

Contains:
- Functions for displaying text (adapted from http://www.technoblogy.com/show?2EA7)
- Functions for updating the display with the temperature, humidity, eCO2 and TVOC levels
  and the traffic light indicators
*/
#include <stdint.h>

#include "fsl_spi_master_driver.h"
#include "fsl_port_hal.h"

#include "SEGGER_RTT.h"
#include "gpio_pins.h"
#include "warp.h"
#include "devSSD1331.h"
#include "utils.h"

volatile uint8_t	inBuffer[32];
volatile uint8_t	payloadBytes[32];


// Global plot parameters
uint8_t x0 = 0;
uint8_t y0 = 50;
uint8_t ForeR = 0x3F, ForeG = 0x3F, ForeB = 0x3F;
uint8_t BackR = 0x00, BackG = 0x00, BackB = 0x00;
uint8_t Scale = 1; // Text size - 2 for big characters



/*
 *	Override Warp firmware's use of these pins and define new aliases.
 */
enum
{
	kSSD1331PinMOSI		= GPIO_MAKE_PIN(HW_GPIOA, 8),
	kSSD1331PinSCK		= GPIO_MAKE_PIN(HW_GPIOA, 9),
	kSSD1331PinCSn		= GPIO_MAKE_PIN(HW_GPIOB, 13),
	kSSD1331PinDC		= GPIO_MAKE_PIN(HW_GPIOA, 12),
	kSSD1331PinRST		= GPIO_MAKE_PIN(HW_GPIOB, 0),
};

// Font is from http://www.technoblogy.com/show?2EA7
static const char font6x8[0x60][6] = {
{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 
{ 0x00, 0x00, 0x5F, 0x00, 0x00, 0x00 }, 
{ 0x00, 0x07, 0x00, 0x07, 0x00, 0x00 }, 
{ 0x14, 0x7F, 0x14, 0x7F, 0x14, 0x00 }, 
{ 0x24, 0x2A, 0x7F, 0x2A, 0x12, 0x00 }, 
{ 0x23, 0x13, 0x08, 0x64, 0x62, 0x00 }, 
{ 0x36, 0x49, 0x56, 0x20, 0x50, 0x00 }, 
{ 0x00, 0x08, 0x07, 0x03, 0x00, 0x00 }, 
{ 0x00, 0x1C, 0x22, 0x41, 0x00, 0x00 }, 
{ 0x00, 0x41, 0x22, 0x1C, 0x00, 0x00 }, 
{ 0x2A, 0x1C, 0x7F, 0x1C, 0x2A, 0x00 }, 
{ 0x08, 0x08, 0x3E, 0x08, 0x08, 0x00 }, 
{ 0x00, 0x80, 0x70, 0x30, 0x00, 0x00 }, 
{ 0x08, 0x08, 0x08, 0x08, 0x08, 0x00 }, 
{ 0x00, 0x00, 0x60, 0x60, 0x00, 0x00 }, 
{ 0x20, 0x10, 0x08, 0x04, 0x02, 0x00 }, 
{ 0x3E, 0x51, 0x49, 0x45, 0x3E, 0x00 }, 
{ 0x00, 0x42, 0x7F, 0x40, 0x00, 0x00 }, 
{ 0x72, 0x49, 0x49, 0x49, 0x46, 0x00 }, 
{ 0x21, 0x41, 0x49, 0x4D, 0x33, 0x00 }, 
{ 0x18, 0x14, 0x12, 0x7F, 0x10, 0x00 }, 
{ 0x27, 0x45, 0x45, 0x45, 0x39, 0x00 }, 
{ 0x3C, 0x4A, 0x49, 0x49, 0x31, 0x00 }, 
{ 0x41, 0x21, 0x11, 0x09, 0x07, 0x00 }, 
{ 0x36, 0x49, 0x49, 0x49, 0x36, 0x00 }, 
{ 0x46, 0x49, 0x49, 0x29, 0x1E, 0x00 }, 
{ 0x00, 0x00, 0x14, 0x00, 0x00, 0x00 }, 
{ 0x00, 0x40, 0x34, 0x00, 0x00, 0x00 }, 
{ 0x00, 0x08, 0x14, 0x22, 0x41, 0x00 }, 
{ 0x14, 0x14, 0x14, 0x14, 0x14, 0x00 }, 
{ 0x00, 0x41, 0x22, 0x14, 0x08, 0x00 }, 
{ 0x02, 0x01, 0x59, 0x09, 0x06, 0x00 }, 
{ 0x3E, 0x41, 0x5D, 0x59, 0x4E, 0x00 }, 
{ 0x7C, 0x12, 0x11, 0x12, 0x7C, 0x00 }, 
{ 0x7F, 0x49, 0x49, 0x49, 0x36, 0x00 }, 
{ 0x3E, 0x41, 0x41, 0x41, 0x22, 0x00 }, 
{ 0x7F, 0x41, 0x41, 0x41, 0x3E, 0x00 }, 
{ 0x7F, 0x49, 0x49, 0x49, 0x41, 0x00 }, 
{ 0x7F, 0x09, 0x09, 0x09, 0x01, 0x00 }, 
{ 0x3E, 0x41, 0x41, 0x51, 0x73, 0x00 }, 
{ 0x7F, 0x08, 0x08, 0x08, 0x7F, 0x00 }, 
{ 0x00, 0x41, 0x7F, 0x41, 0x00, 0x00 }, 
{ 0x20, 0x40, 0x41, 0x3F, 0x01, 0x00 }, 
{ 0x7F, 0x08, 0x14, 0x22, 0x41, 0x00 }, 
{ 0x7F, 0x40, 0x40, 0x40, 0x40, 0x00 }, 
{ 0x7F, 0x02, 0x1C, 0x02, 0x7F, 0x00 }, 
{ 0x7F, 0x04, 0x08, 0x10, 0x7F, 0x00 }, 
{ 0x3E, 0x41, 0x41, 0x41, 0x3E, 0x00 }, 
{ 0x7F, 0x09, 0x09, 0x09, 0x06, 0x00 }, 
{ 0x3E, 0x41, 0x51, 0x21, 0x5E, 0x00 }, 
{ 0x7F, 0x09, 0x19, 0x29, 0x46, 0x00 }, 
{ 0x26, 0x49, 0x49, 0x49, 0x32, 0x00 }, 
{ 0x03, 0x01, 0x7F, 0x01, 0x03, 0x00 }, 
{ 0x3F, 0x40, 0x40, 0x40, 0x3F, 0x00 }, 
{ 0x1F, 0x20, 0x40, 0x20, 0x1F, 0x00 }, 
{ 0x3F, 0x40, 0x38, 0x40, 0x3F, 0x00 }, 
{ 0x63, 0x14, 0x08, 0x14, 0x63, 0x00 }, 
{ 0x03, 0x04, 0x78, 0x04, 0x03, 0x00 }, 
{ 0x61, 0x59, 0x49, 0x4D, 0x43, 0x00 }, 
{ 0x00, 0x7F, 0x41, 0x41, 0x41, 0x00 }, 
{ 0x02, 0x04, 0x08, 0x10, 0x20, 0x00 }, 
{ 0x00, 0x41, 0x41, 0x41, 0x7F, 0x00 }, 
{ 0x04, 0x02, 0x01, 0x02, 0x04, 0x00 }, 
{ 0x40, 0x40, 0x40, 0x40, 0x40, 0x00 }, 
{ 0x00, 0x03, 0x07, 0x08, 0x00, 0x00 }, 
{ 0x20, 0x54, 0x54, 0x78, 0x40, 0x00 }, 
{ 0x7F, 0x28, 0x44, 0x44, 0x38, 0x00 }, 
{ 0x38, 0x44, 0x44, 0x44, 0x28, 0x00 }, 
{ 0x38, 0x44, 0x44, 0x28, 0x7F, 0x00 }, 
{ 0x38, 0x54, 0x54, 0x54, 0x18, 0x00 }, 
{ 0x00, 0x08, 0x7E, 0x09, 0x02, 0x00 }, 
{ 0x18, 0xA4, 0xA4, 0x9C, 0x78, 0x00 }, 
{ 0x7F, 0x08, 0x04, 0x04, 0x78, 0x00 }, 
{ 0x00, 0x44, 0x7D, 0x40, 0x00, 0x00 }, 
{ 0x20, 0x40, 0x40, 0x3D, 0x00, 0x00 }, 
{ 0x7F, 0x10, 0x28, 0x44, 0x00, 0x00 }, 
{ 0x00, 0x41, 0x7F, 0x40, 0x00, 0x00 }, 
{ 0x7C, 0x04, 0x78, 0x04, 0x78, 0x00 }, 
{ 0x7C, 0x08, 0x04, 0x04, 0x78, 0x00 }, 
{ 0x38, 0x44, 0x44, 0x44, 0x38, 0x00 }, 
{ 0xFC, 0x18, 0x24, 0x24, 0x18, 0x00 }, 
{ 0x18, 0x24, 0x24, 0x18, 0xFC, 0x00 }, 
{ 0x7C, 0x08, 0x04, 0x04, 0x08, 0x00 }, 
{ 0x48, 0x54, 0x54, 0x54, 0x24, 0x00 }, 
{ 0x04, 0x04, 0x3F, 0x44, 0x24, 0x00 }, 
{ 0x3C, 0x40, 0x40, 0x20, 0x7C, 0x00 }, 
{ 0x1C, 0x20, 0x40, 0x20, 0x1C, 0x00 }, 
{ 0x3C, 0x40, 0x30, 0x40, 0x3C, 0x00 }, 
{ 0x44, 0x28, 0x10, 0x28, 0x44, 0x00 }, 
{ 0x4C, 0x90, 0x90, 0x90, 0x7C, 0x00 }, 
{ 0x44, 0x64, 0x54, 0x4C, 0x44, 0x00 }, 
{ 0x00, 0x08, 0x36, 0x41, 0x00, 0x00 }, 
{ 0x00, 0x00, 0x77, 0x00, 0x00, 0x00 }, 
{ 0x00, 0x41, 0x36, 0x08, 0x00, 0x00 }, 
{ 0x02, 0x01, 0x02, 0x04, 0x02, 0x00 }, 
{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00 },
};


static int
writeCommand(uint8_t commandByte)
{
	spi_status_t status;

	/*
	 *	Drive /CS low.
	 *
	 *	Make sure there is a high-to-low transition by first driving high, delay, then drive low.
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);
	OSA_TimeDelay(10);
	GPIO_DRV_ClearPinOutput(kSSD1331PinCSn);

	/*
	 *	Drive DC low (command).
	 */
	GPIO_DRV_ClearPinOutput(kSSD1331PinDC);

	payloadBytes[0] = commandByte;
	status = SPI_DRV_MasterTransferBlocking(0	/* master instance */,
					NULL		/* spi_master_user_config_t */,
					(const uint8_t * restrict)&payloadBytes[0],
					(uint8_t * restrict)&inBuffer[0],
					1		/* transfer size */,
					1000		/* timeout in microseconds (unlike I2C which is ms) */);

	/*
	 *	Drive /CS high
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);

	return status;
}


// writeManyCommandsFast is similar to writeCommand but doesn't drive /CS and DC low
// before the command or drive /CS high afterwards
// and it writes several bytes at a time
static int
writeManyCommandsFast(uint8_t* commandBytes, uint8_t no_bytes)
{
	// We assume that /CS and DC have been driven low already
	spi_status_t status;

	status = SPI_DRV_MasterTransferBlocking(0	/* master instance */,
					NULL		/* spi_master_user_config_t */,
					(const uint8_t * restrict)commandBytes,
					(uint8_t * restrict)&inBuffer[0],
					no_bytes		/* transfer size */,
					1000		/* timeout in microseconds (unlike I2C which is ms) */);

	return status;
}


int
devSSD1331init(void)
{
	/*
	 *	Override Warp firmware's use of these pins.
	 *
	 *	Re-configure SPI to be on PTA8 and PTA9 for MOSI and SCK respectively.
	 */
	PORT_HAL_SetMuxMode(PORTA_BASE, 8u, kPortMuxAlt3);
	PORT_HAL_SetMuxMode(PORTA_BASE, 9u, kPortMuxAlt3);

	enableSPIpins();

	/*
	 *	Override Warp firmware's use of these pins.
	 *
	 *	Reconfigure to use as GPIO.
	 */
	PORT_HAL_SetMuxMode(PORTB_BASE, 13u, kPortMuxAsGpio);
	PORT_HAL_SetMuxMode(PORTA_BASE, 12u, kPortMuxAsGpio);
	PORT_HAL_SetMuxMode(PORTB_BASE, 0u, kPortMuxAsGpio);


	/*
	 *	RST high->low->high.
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinRST);
	OSA_TimeDelay(100);
	GPIO_DRV_ClearPinOutput(kSSD1331PinRST);
	OSA_TimeDelay(100);
	GPIO_DRV_SetPinOutput(kSSD1331PinRST);
	OSA_TimeDelay(100);

	/*
	 *	Initialization sequence, borrowed from https://github.com/adafruit/Adafruit-SSD1331-OLED-Driver-Library-for-Arduino
	 */
	writeCommand(kSSD1331CommandDISPLAYOFF);	// 0xAE
	writeCommand(kSSD1331CommandSETREMAP);		// 0xA0
	writeCommand(0x73);             // Enabled vertical increment mode
	writeCommand(kSSD1331CommandSTARTLINE);		// 0xA1
	writeCommand(0x0);
	writeCommand(kSSD1331CommandDISPLAYOFFSET);	// 0xA2
	writeCommand(0x0);
	writeCommand(kSSD1331CommandNORMALDISPLAY);	// 0xA4
	writeCommand(kSSD1331CommandSETMULTIPLEX);	// 0xA8
	writeCommand(0x3F);				// 0x3F 1/64 duty
	writeCommand(kSSD1331CommandSETMASTER);		// 0xAD
	writeCommand(0x8E);
	writeCommand(kSSD1331CommandPOWERMODE);		// 0xB0
	writeCommand(0x0B);
	writeCommand(kSSD1331CommandPRECHARGE);		// 0xB1
	writeCommand(0x31);
	writeCommand(kSSD1331CommandCLOCKDIV);		// 0xB3
	writeCommand(0xF0);				// 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
	writeCommand(kSSD1331CommandPRECHARGEA);	// 0x8A
	writeCommand(0x64);
	writeCommand(kSSD1331CommandPRECHARGEB);	// 0x8B
	writeCommand(0x78);
	writeCommand(kSSD1331CommandPRECHARGEA);	// 0x8C
	writeCommand(0x64);
	writeCommand(kSSD1331CommandPRECHARGELEVEL);	// 0xBB
	writeCommand(0x3A);
	writeCommand(kSSD1331CommandVCOMH);		// 0xBE
	writeCommand(0x3E);
	writeCommand(kSSD1331CommandMASTERCURRENT);	// 0x87
	writeCommand(0x06);
	writeCommand(kSSD1331CommandCONTRASTA);		// 0x81
	writeCommand(0x91);
	writeCommand(kSSD1331CommandCONTRASTB);		// 0x82
	writeCommand(0x50);
	writeCommand(kSSD1331CommandCONTRASTC);		// 0x83
	writeCommand(0x7D);
	writeCommand(kSSD1331CommandDISPLAYON);		// Turn on oled panel
	// SEGGER_RTT_WriteString(0, "\r\n\tDone with initialization sequence...\n");

	/*
	 *	To use fill commands, you will have to issue a command to the display to enable them. See the manual.
	 */
	writeCommand(kSSD1331CommandFILL);
	writeCommand(0x01);
	// SEGGER_RTT_WriteString(0, "\r\n\tDone with enabling fill...\n");

	/*
	 *	Clear Screen
	 */
	writeCommand(kSSD1331CommandCLEAR);
	writeCommand(0x00);
	writeCommand(0x00);
	writeCommand(0x5F);
	writeCommand(0x3F);
	// SEGGER_RTT_WriteString(0, "\r\n\tDone with screen clear...\n");


	// Drive DC low (we will only be sending commands and not data so DC can be kept low)
	/*
	 *	Drive DC low (command).
	 */
	GPIO_DRV_ClearPinOutput(kSSD1331PinDC);

	return 0;
}


void PlotChar (uint8_t ch) {
	/* Draws a single character of text (ch) on the display
	   at location given by (x0, y0)
	*/
	
	/*
	 *	Drive /CS low.
	 *
	 *	Make sure there is a high-to-low transition by first driving high, then drive low.
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);
	GPIO_DRV_ClearPinOutput(kSSD1331PinCSn);


	for (uint8_t c = 0 ; c < 6; c++) {      // Column range
	uint8_t bits = font6x8[ch-32][c];
	uint8_t r = 0;

	// Loop until bits is 0
	while (bits) {
		// Increment r to get the length of the first vertical line
		while ((bits & 1) == 0) {r++; bits = bits>>1; } // e.g. if letter T, r=0
		uint8_t on = (r)*Scale; // on = 0
		
		while ((bits & 1) != 0) {r++; bits = bits>>1; } // r = 1
		uint8_t off = (r)*Scale-1;  // off = 1

		for (int i=0; i<Scale; i++) {
			uint8_t h = x0+c*Scale+i;

			unsigned char lineCMD[8];
			lineCMD[0] = kSSD1331CommandDRAWLINE;  // Draw line
			lineCMD[1] = h;       // Start column
			lineCMD[2] = y0+on;   // Start row
			lineCMD[3] = h;       // End column
			lineCMD[4] = y0+off;  // End row (e.g. 0)
			// White colour
			lineCMD[5] = 0x3F;  
			lineCMD[6] = 0x3F;
			lineCMD[7] = 0x3F;
			writeManyCommandsFast(lineCMD, 8);

		}
	}
	}
	/*
	 *	Drive /CS high
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);

	x0 = x0+6*Scale;
}


void
clearTextSSD1331(uint8_t start_x, uint8_t start_y, uint8_t no_chars)
{
	/* Clear text from screen

	   Height of window to clear = 8 (one char height)
	   Width of window to clear = no_chars * 6
	*/

	
	//	Drive /CS low.
	//	Make sure there is a high-to-low transition by first driving high, then drive low.
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);
	GPIO_DRV_ClearPinOutput(kSSD1331PinCSn);

	// Create and write the clear command
	unsigned char clearCMD[5];
	clearCMD[0] = kSSD1331CommandCLEAR;
	clearCMD[1] = start_x;  // start x
	clearCMD[2] = start_y;  // start y
	clearCMD[3] = start_x + (no_chars * kSSD1331FontWidth);  // end x
	clearCMD[4] = start_y + kSSD1331FontHeight;  // end y
	writeManyCommandsFast(clearCMD, 5);

	// Drive /CS high
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);
}


/* Writes a line of text on the OLED display */
int
writeTextSSD1331(char* text, uint8_t text_length, uint8_t start_x, uint8_t start_y)
{
	// Set the cursor (x0, y0) to the start point specified
	x0 = start_x;
	y0 = start_y;

	// Print each character in `text` to the display
	for (uint8_t i = 0; i < text_length; i++) {
		PlotChar(text[i]);
	}

	return 0;
}

void SetupUserInterface()
{
	writeTextSSD1331("Temp: 00.0 C", 12, 0, 5);
	writeTextSSD1331("Humi: 00.0 %", 12, 0, 20);
	writeTextSSD1331("tVOC: 0000 ppb", 14, 0, 35);
	writeTextSSD1331("eCO2: 0000 ppm", 14, 0, 50);
}


int updateTempDisplay(uint16_t temp)
{
	/*
	Updates the temperature displayed on OLED
	
	`temp` is assumed to be a 4 digit integer
	(e.g. 2093 = 20.93 deg C)

	The rounded value (to 1dp) is displayed.

	Returns 0, 1, or 2 depending on whether the temperature is
	good, OK or bad respectively
	*/
	uint16_t temp_rounded;
	float temp_float;
	char temp_disp[5]; 

	// Convert the temperature to a char array for displaying on OLED
	temp_rounded = round(temp, 10);  // round to nearest 10 (3sf)
	temp_float = (float)temp_rounded / 100;  // convert to float
	_float_to_char(temp_float, temp_disp, 5);  // convert to char (will be 5 chars)

	clearTextSSD1331(36, 5, 4); // clear the existing temperature shown
	// Write new temperature to display we display only 4 chars (last char will
	// be 0 or invalid)
	writeTextSSD1331(temp_disp, 4, 36, 5);  

	// Update traffic light
	// Good: 18-24 deg
	if (temp >= 1800 && temp <= 2400) {
		drawTrafficLight('t', 'g');
		return 0;
	}
	// Medium: 16-18 or 24-26
	else if (temp >= 1600 && temp <= 2600) {
		drawTrafficLight('t', 'a');
		return 1;
	}
	// Bad: <16 or >26
	else {
		drawTrafficLight('t', 'r');
		return 2;
	}


}


int updateHumDisplay(uint32_t hum)
{
	/*
	Updates the humidity displayed on OLED
	
	`hum` is assumed to be a 5 digit integer
	(e.g. 75623 = 75.623 %)

	The rounded value (to 1dp) is displayed.

	Returns 0, 1, or 2 depending on whether the humidity is
	good, OK or bad respectively
	*/
	uint32_t hum_rounded;
	float hum_float;
	char hum_disp[5]; 

	// Write 100 to display if get 100% humidity
	if (hum == 100000) {
		clearTextSSD1331(36, 20, 4);
		writeTextSSD1331("100 ", 4, 36, 20);
	}

	// Otherwise, convert the humidity to a char array for displaying on OLED
	else {
		hum_rounded = round(hum, 100); // round to nearest 100
		hum_float = (float)hum_rounded / 1000; // convert to float
		_float_to_char(hum_float, hum_disp, 5);  // convert to char (will be 5 chars)
		clearTextSSD1331(36, 20, 4); // clear the existing temperature shown
		writeTextSSD1331(hum_disp, 4, 36, 20);  // we display only first 4 chars
	}

	// Update traffic light
	// Thresholds based on: https://www.anglianhome.co.uk/goodtobehome/guides/how-to-spot-and-remedy-high-humidity-in-your-home/
	// Good: 40 - 60 %
	if (hum >= 40000 && hum <= 60000) {
		drawTrafficLight('h', 'g');
		return 0;
	}
	// Medium: 30-40 or 60-80
	else if (hum >= 30000 && hum <= 80000) {
		drawTrafficLight('h', 'a');
		return 1;
	}
	// Bad: < 30 or > 80
	else {
		drawTrafficLight('h', 'r');
		return 2;
	}

}


int updateCO2Display(uint16_t eco2)
{
	/*
	Updates the eCO2 displayed on OLED

	Returns 0, 1, or 2 depending on whether the eCO2 is
	good, OK or bad respectively
	*/

	// Write 0 to display if measurement is 0
	if (eco2 == 0) {
		clearTextSSD1331(36, 50, 4);
		writeTextSSD1331("0", 1, 36, 50);
	}
	// Otherwise convert the integer measurement to char array
	// so it can be displayed
	else {
		uint8_t reading_len = 3;
		if (eco2 >= 1000) {
			reading_len = 4;
		}

		char eco2_disp[reading_len];
		_int_to_char(eco2, eco2_disp, reading_len);  // either 3 or 4 chars
		clearTextSSD1331(36, 50, 4);
		writeTextSSD1331(eco2_disp, reading_len, 36, 50);
	}

	// Update traffic light
	// Thresholds based on https://www.kane.co.uk/knowledge-centre/what-are-safe-levels-of-co-and-co2-in-rooms
	// Good: <1000ppm
	if (eco2 < 1000) {
		drawTrafficLight('c', 'g');
		return 0;
	}
	// Medium: 1000 - 2000 ppm
	else if (eco2 < 2000) {
		drawTrafficLight('c', 'a');
		return 1;
	}
	// Bad: >2000 ppm
	else {
		drawTrafficLight('c', 'r');
		return 2;
	}

}


int updateTVOCDisplay(uint16_t tvoc)
{
	/*
	Updates the TVOC displayed on OLED

	Returns 0, 1, or 2 depending on whether the TVOC is
	good, OK or bad respectively
	*/

	// Write 0 to display if measurement is 0
	if (tvoc == 0) {
		clearTextSSD1331(36, 35, 4);
		writeTextSSD1331("0", 1, 36, 35);
	}
	// Otherwise convert the integer measurement to char array
	// so it can be displayed
	else {
		uint8_t reading_len = 3;
		if (tvoc < 10) {
			reading_len = 1;
		}
		else if (tvoc < 100) {
			reading_len = 2;
		}
		else if (tvoc < 1000) {
			reading_len = 3;
		}
		else {
			reading_len = 4;
		}

		char tvoc_disp[reading_len];
		_int_to_char(tvoc, tvoc_disp, reading_len);
		clearTextSSD1331(36, 35, 4); // clear existing TVOC reading
		writeTextSSD1331(tvoc_disp, reading_len, 36, 35);
	}
	

	// Update traffic light
	// Thresholds based on: https://www.airthings.com/en-gb/what-is-voc
	// Good: < 250 ppb
	if (tvoc < 250) {
		drawTrafficLight('v', 'g');
		return 0;
	}
	// Medium: 250 - 1000 ppb
	else if (tvoc < 1000) {
		drawTrafficLight('v', 'a');
		return 1;
	}
	// Bad: > 1000ppb (note sensor measures up to 1187 ppb only)
	else {
		drawTrafficLight('v', 'r');
		return 2;
	}
}


// Define the 8x8 symbols (only circle at the moment)
static const char symbols[1][8] = {
	{ 0b00011000, 0b01111110, 0b01111110, 0b11111111, 0b11111111, 0b01111110, 0b01111110, 0b00011000 },
};


void drawSymbol (char colour, uint8_t symbol_type) {
	// Set colour to g for green, a for amber, r for red
	// Set symbol_type to c for circle
	uint8_t red = 0x00;
	uint8_t green = 0x00;
	uint8_t blue = 0x00;
	uint8_t symbol_idx = 0;
	// Set RGB colours
	if (colour == 'g') {
		green = 0x3F;
	}
	else if (colour == 'a') {
		red = 0xFF;
		green = 0x30;
		blue = 0x00;
	}
	else if (colour == 'r') {
		red = 0xFF;
	}

	if (symbol_type == 'c') {
		symbol_idx = 0;
	}

	/*
	 *	Drive /CS low.
	 *
	 *	Make sure there is a high-to-low transition by first driving high, delay, then drive low.
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);
	GPIO_DRV_ClearPinOutput(kSSD1331PinCSn);


	for (uint8_t c = 0 ; c < 8; c++) {      // Column range
	uint8_t bits = symbols[symbol_idx][c];
	uint8_t r = 0;

	// Loop until bits is 0
	while (bits) {
		// Increment r to get the length of the first vertical line
		while ((bits & 1) == 0) {r++; bits = bits>>1; }
		uint8_t on = (r)*Scale;
		
		while ((bits & 1) != 0) {r++; bits = bits>>1; }
		uint8_t off = (r)*Scale-1;

		for (int i=0; i<Scale; i++) {
			uint8_t h = x0+c*Scale+i;
			// Create command for drawing line and write it
			unsigned char lineCMD[8];
			lineCMD[0] = kSSD1331CommandDRAWLINE;  // Draw line
			lineCMD[1] = h;       // Start column
			lineCMD[2] = y0+on;   // Start row
			lineCMD[3] = h;       // End column
			lineCMD[4] = y0+off;  // End row
			lineCMD[5] = red;
			lineCMD[6] = green;
			lineCMD[7] = blue;
			writeManyCommandsFast(lineCMD, 8);

		}
	}
	}
	/*
	 *	Drive /CS high
	 */
	GPIO_DRV_SetPinOutput(kSSD1331PinCSn);

	x0 = x0+6*Scale;
}


void drawTrafficLight(char metric, char colour) {
	/* Update traffic light of metric specified
	with the colour specified */
	
	x0 = 88; // start column of traffic lights
	
	// Temperature row
	if (metric == 't') {
		y0 = 5;
	}
	// Humidity row
	else if (metric == 'h') {
		y0 = 20;
	}
	// TVOC row
	else if (metric == 'v') {
		y0 = 35;
	}
	// eCO2 row
	else if (metric == 'c') {
		y0 = 50;
	}
	
	// Draw the circle symbol of specified colour
	drawSymbol(colour, (uint8_t)'c');

}

