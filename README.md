# Indoor Environment Monitor

**Jason Quach (jq245), St Catharine's College**

An indoor environment / air quality monitoring system implemented using the FRDMKL03 development board. It is designed to be a cost effective system which allows people working from home to monitor their working environment. Four key metrics (temperature, humidity, eCO2 and TVOC) are measured and displayed to the user on an OLED display. A simple user interface with a traffic light system makes it easy to interpret the readings and additional auditory alerts from a piezo buzzer are sounded when the readings indicate a poor indoor environment.

## Hardware overview

- A Bosch Sensortec BME680 sensor is used for temperature and humidity measurements
- A ScioSense CCS811 sensor is used for equivalent carbon dioxide (eCO2) and Total Volatile Organic Compounds (TVOC) measurements
- The measurements are displayed on an 0.96" mini OLED display (using SSD1331 driver)
- A piezo buzzer gives auditory alerts

## Software overview

The software implementation has been based on the Warp-firmware developed by Cambridge Physical Computation Laboratory (https://github.com/physical-computation/Warp-firmware). 
Changes made from the original Warp-firmware can be seen from a diff to the commit `3179580`. The main source code is located in `Warp-firmware/src/boot/ksdk1.1.0`. The code can be compiled using the script `Warp-firmware/build/ksdk1.1/build.sh`. For full details see the original Warp-firmware repository.

### BME680 driver ###
**`devBME680.c, devBME680.h`**

The existing BME680 driver included with the Warp firmware was used as a base. This only provided functions for obtaining the raw temperature/humidity values. So functions were added for:
- Calculating the temperature in deg C and the relative humidity % (these were based on those provided in the BME680 datasheet and in the official driver github.com/BoschSensortec/BME680_driver)
- Getting the readings directly rather than printing them out.

### CCS811 driver ###
**`devCCS811.c, devCCS811.h`**

The existing CCS811 driver included with the Warp firmware was used as a base. Additional functions were added for:
- Writing temperature and humidity values to the ENV_DATA register for compensating the gas readings due to temperature/humidity changes
- Getting the readings directly rather than printing them out. 

### SSD1331 OLED driver implementation ###
**`devSSD1331.c, devSSD1331.h`**

The existing SSD1331 driver included with the Warp firmware was used as a base.

Text and symbol display capabilities were added:
- Adapted from an Arduino inplementation here: http://www.technoblogy.com/show?2EA7
- Text is drawn as a series of lines (using the Graphics Acceleration Commands) rather than writing directly to the graphics data memory as this was found to be much faster (see function `PlotChar`)
- Symbols (only circles at the moment) are drawn in a similar manner (see `drawSymbol` function)

Functions for updating the display:
- Functions for updating the temperature, humidity, TVOC and eCO2 values on the display are found here
- The measured values from the BME680/CCS811 are all in integer format. The temp/humidity are rounded and converted to char arrays so they can be displayed correctly. The TVOC/eCO2 do not require rounding but still need to be converted to char arrays. The functions for rounding/conversion are in the `utils.c` file.
- The red/amber/green 'traffic light' indicators are also updated here with the threshold values defined within the relevant functions

### Miscellaneous functions ###
**`utils.c, utils.h`**

Functions for conversion of floats/ints to char and for rounding of integers.

### Main functions ###
The main loop is located in **`warp-kl03-ksdk1.1-boot.c`.**
- Code not relevant to this project (e.g. for the Warp menu) was removed
- Sensor readings are obtained and displayed on the OLED
- The piezo buzzer is sounded if any readings in the the 'red' zone
- Readings are taken approximately every 1 second
